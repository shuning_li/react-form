import React, {Component} from 'react';
import './myProfile.less';

class MyProfile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      user: {
        name: '',
        gender: 'male',
        description: ''
      },
      checked: false
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleGenderChange = this.handleGenderChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheckedBoxChange = this.handleCheckedBoxChange.bind(this);
  }

  handleNameChange(e) {
    this.setState({
      ...this.state,
      user: {
        ...this.state.user,
        name: e.target.value
      }
    })
  }

  handleGenderChange(e) {
    this.setState({
      ...this.state,
      user: {
        ...this.state.user,
        gender: e.target.value
      }
    })
  }

  handleDescriptionChange(e) {
    this.setState({
      ...this.state,
      user: {
        ...this.state.user,
        description: e.target.value
      }
    })
  }

  handleCheckedBoxChange(e) {
    this.setState({
      ...this.state,
      checked: e.target.checked
    })
  }

  handleSubmit(e) {
    e.preventDefault();
    const { checked } = this.state;
    if (checked) {
      console.log(this.state.user);
    }
  }

  render() {
    const { user, checked } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <section>
          <label htmlFor="name">Name</label>
          <input onChange={this.handleNameChange} id="name" value={user.name} type="text"/>
        </section>
        <section>
          <label htmlFor="gender"></label>
          <select onChange={this.handleGenderChange} value={user.gender} id="gender">
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </section>
        <section>
          <label htmlFor="description">Description</label>
          <textarea onChange={this.handleDescriptionChange} value={user.description}></textarea>
        </section>
        <section>
          <input onChange={this.handleCheckedBoxChange} checked={checked} type="checkbox"/> <span>I have read the terms of conduct</span>
        </section>
        <button>Submit</button>
      </form>
    );
  }
}

export default MyProfile;


